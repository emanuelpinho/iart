% Escreva um programa para interpretar frases afirmativas. 
% Se a frase estiver correcta deve ser escrita a palavra "correcto". 
% Se estiver incorrecta (erro de sintaxe ou semantica), deve ser 
% escrita a palavra "incorrecto". 
% O programa determina tambem a estrutura da frase. 
% Exemplos: 
% os rapazes jogam futebol. concordo (afirmacao verdadeira)
% os caes e os gatos gostam de bolachas.
% o elefante come amendoins. 
% o rui gosta de morangos. discordo (afirmacao falsa) 
% os rapazes joga futebol. incorrecto (erro de sintaxe) 
% o elefante joga futebol. incorrecto (erro de semantica) 
% Inclua tambem a interpretacao (e resposta) de frases interrogativas
% quem joga futebol ?
% quantos humanos gostam de morangos ?
% quais sao os humanos que gostam de bolachas ? 


% Quais são os pratos típicos do Porto?
% Quem inventou a francesinha?
% E o bacalhau à Gomes de Sá?
% Quantos pratos de bacalhau existem?
% O que se pode cozinhar com pão?
% De onde são as migas?
% O arroz de cabidela leva sangue?

inicio:- write('Escreva uma frase: '),nl,
    %current_prolog_flag(backquoted_string,V),
    %set_prolog_flag(backquoted_string,true),
    read(Frase),
    %set_prolog_flag(backquoted_string,V),
    transf_lista(Frase,Lista_Pal),
    verifica(Lista_Pal).
 
transf_lista(Frase,Lista_Pal):-
    string_to_list(Frase,Lista_char),
    faz_palavras(Lista_char,[],Lista_Pal).
    faz_palavras([],L_char1,[Pal1]):-
    atom_chars(Pal1,L_char1).
 
faz_palavras([32|T],L_char1,[Pal1|T1]):-
    atom_chars(Pal1,L_char1),
    faz_palavras(T,[],T1).
faz_palavras([X|T],L_char,L_Pal):-
    append(L_char,[X],L_char1),
    faz_palavras(T,L_char1,L_Pal).
 
verifica(ListaPal):-
    ( retract(erro_semantico) ; true ),
    ( ( verifica_frase(Estrutura,ListaPal,[]), write(Estrutura) ) ; ( erro_semantico, write('erro semantico') ) ; write('erro de sintaxe') ).
 
%%% frase interrogativa
verifica_frase(frase(SI,SV))-->
    sintagma_interrogativo(SI,Q,N,Accao1,Objecto1),
    sintagma_verbal(SV,N,_,Accao,Objecto),
    { responde(Q,Accao1,Objecto1,Accao,Objecto) }.
 
responde(Q,Accao1,Objecto1,Accao,Objecto):-
    var(Accao1), Predicado=..[Accao, Sujeito,Objecto],
    findall(Sujeito, Predicado,Lista), 
    ( ( Q=qual,write(Lista) ) ; ( length(Lista,Nlista),write(Nlista) ) ), nl.
 
responde(Q,Accao1,Objecto1,Accao,Objecto):-
    nonvar(Accao1), %   True if Accao1 currently is a free variable
    Predicado=..[Accao,Sujeito,Objecto], % Predicado passa a ser uma lista com aqueles argumentos
    Predicado1=..[Accao1,Sujeito,Objecto1],
    findall(Sujeito, ( Predicado, Predicado1 ), Lista),  % Procura todos os Sujeito que  satisfazem Predicado e Predicado1 e colocam em Lista
    ( ( Q=qual,write(Lista) )
    ; ( length(Lista,Nlista),write(Nlista) )
    ), nl.
 
sintagma_interrogativo(pronome_inter(I),Q,N,Accao,Objecto)-->
    pron_inter(N-_,I,Q),
    sintagma_nom_inter(SNI,N,Accao,Objecto).
   
sintagma_interrogativo(pronome_inter(I),Q,N,_,_)-->
    pron_inter(N-_,I,Q).
 
sintagma_nom_inter(sintg_nominal(verbo(V),det(D),nome(Nome),pronome(_,que)),N,ser,Objecto)-->
    (verbo(N,V,_,ser) ;{ true} ),
    determinante(N-G,D),
    nome(N-G,Nome,Objecto),
    pronome(_,que).
 
sintagma_nom_inter(sintg_nominal(nome(Nome)),N,_,Objecto)-->
    nome(N-G,Nome,Objecto).
 
%%% frase afirmativa
verifica_frase(frase(SN,SV))-->
    sintagma_nominal(SN,N,Sujeito),
    sintagma_verbal(SV,N,Sujeito,Accao,Objecto),
    { concorda_frase(Accao,Sujeito,Objecto),nl }.
    concorda_frase(_,[],_):- write(concordo).
 
concorda_frase(Accao,[Sujeito1|OSuj],Objecto):-
    ( Predicado=..[Accao,Sujeito1,Objecto], Predicado,
    concorda_frase(Accao,OSuj,Objecto) )
    ; write(discordo).
 
concorda_frase(Accao,Sujeito,Objecto):-
    ( ( Predicado=..[Accao,Sujeito,Objecto], Predicado, write(concordo) )
    ; write(discordo) ).
 
sintagma_nominal(SN,p,[Sujeito1,Sujeito2])-->
    sintagma_nom1(SN1,N1,Sujeito1),[e],
    sintagma_nom1(SN2,N2,Sujeito2),
    { SN1=..[_|L1], SN2=..[_|L2],
    append(L1,[e|L2],L),
    SN=..[sintg_nominal|L]
    }.
 
sintagma_nominal(SN,N,Sujeito)-->
    sintagma_nom1(SN,N,Sujeito).
 
sintagma_nom1(sintg_nominal(det(D),nome(Nome)),N,Sujeito)-->
    determinante(N-G,D),
    nome(N-G,Nome,Sujeito).
 
sintagma_nom1(sintg_nominal(nome(Nome)),N,Sujeito)-->
    nome(N-_,Nome,Sujeito).
 
sintagma_verbal(sintg_verbal(verbo(V),SN),N,Nome,gostar,Obj)-->
    verbo(N,V,Nome,gostar),preposicao(N1-G1,P),
    nome(N1-G1,Nome1,Obj),
    {SN=sintg_nominal(prep(P),nome(Nome1))}.
 
sintagma_verbal(sintg_verbal(verbo(V),SN),N,Nome,Accao,Obj)-->
    verbo(N,V,Nome,Accao),
    sintagma_nominal(SN,_,Obj).
 
pron_inter(s-_,quem,qual)-->[quem].
pron_inter(p-_,quais,qual)-->[quais].
pron_inter(p-m,quantos,quant)-->[quantos].
pron_inter(p-f,quantas,quant)-->[quantas].
pronome(_,que)-->[que].
determinante(s-m,o)-->[o].
determinante(p-m,os)-->[os].
determinante(s-f,a)-->[a].
preposicao(_,de)--> [de].
preposicao(s-f,da)-->[da].
nome(p-m,rapazes,rapaz)-->[rapazes].
nome(s-m,rui,rui)-->[rui].
nome(s-m,luis,luis)-->[luis].
nome(s-f,rita,rita)-->[rita].
nome(s-f,ana,ana)-->[ana].
nome(s-f,maria,maria)-->[maria].
nome(s-m,elefante,elefante)-->[elefante].
nome(p-m,caes,cao)-->[caes].
nome(p-m,gatos,gato)-->[gatos].
nome(s-m,cao,cao)-->[cao].
nome(s-m,gato,gato)-->[gato].
nome(s-m,futebol,futebol)-->[futebol].
nome(p-m,morangos,morango)-->[morangos].
nome(p-m,amendoins,amendoim)-->[amendoins].
nome(p-m,bolachas,bolacha)-->[bolachas].
nome(p-m,humanos,humano)-->[humanos].
nome(p-f,pessoas,humano)-->[pessoas].
verbo(s,joga,Sujeito,jogar)-->[joga],{humano(Sujeito) ; (assert(erro_semantico), fail)}.
verbo(p,jogam,Sujeito,jogar)-->[jogam],{humano(Sujeito) ; (assert(erro_semantico), fail)}.
verbo(s,gosta,_,gostar)-->[gosta].
verbo(p,gostam,_,gostar)-->[gostam].
verbo(s,come,_,comer)-->[come].
verbo(p,comem,_,comer)-->[comem].
verbo(p,sao,_,ser)-->[sao].
 
humano(rapaz).
humano(rui).
humano(maria).
humano(rita).
humano(ana).
humano(luis).
humano(humano).
humano([]).
humano([H|T]):-humano(H),humano(T).
 
jogar(rapaz,futebol).
jogar(rui,futebol).
gostar(luis,morango).
gostar(rita,morango).
gostar(ana,morango).
comer(elefante,amendoim).
gostar(rui,maria).
gostar(cao,bolacha).
gostar(gato,bolacha).
 
ser(rui,rapaz).
ser(X,humano):- humano(X).