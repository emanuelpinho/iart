:- dynamic erro/1.

inicio:-
    write('Escreva uma frase: '),nl,
    read(Frase),
    transf_lista(Frase,Lista_Pal),
    verifica_frase(_Q,Q2,_Sujeito,Sujeito2,_Accao, Accao2,_Objeto, Objeto2, Lista_Pal,[]),
    ciclo_de_jogo(Q2,_Q3,Sujeito2,_Sujeito3,Accao2, _Accao3,Objeto2, _Objeto3).

ciclo_de_jogo(Q,Q2,Sujeito,Sujeito2,Accao, Accao2,Objeto, Objeto2):- nl,
	write('Escreva uma frase: '),nl,
	read(Frase), %fread(s,0,0,Frase)
	transf_lista(Frase,Lista_Pal),
    verifica_frase(Q,Q2,Sujeito,Sujeito2,Accao, Accao2,Objeto, Objeto2, Lista_Pal,[]),
    ciclo_de_jogo(Q2,_Q3,Sujeito2,_Sujeito3,Accao2, _Accao3,Objeto2, _Objeto3).

verifica_frase(Q,_Q2,_Sujeito,_Sujeito2,Accao, _Accao2,_Objeto, _Objeto2)--> 
    e,
    verifica_encadeadas(_G-N, Sujeito3, Objeto3),
    {resposta(Q, Sujeito3, Accao, Objeto3, Lista)},
    ligador(Q, Sujeito3, Accao, Objeto3, N, Lista).

verifica_frase(_Q,Q2,_Sujeito,Sujeito2,_Accao, Accao2,_Objeto, Objeto2)-->
    analise(Q2,Sujeito2, Accao2, Objeto2, N),
    {resposta(Q2, Sujeito2, Accao2, Objeto2, Lista)},
    ligador(Q2, Sujeito2, Accao2, Objeto2, N, Lista).

verifica_encadeadas(G-N, Sujeito2, _Objeto2)-->
    sujeito(G-N,Sujeito2).

verifica_encadeadas(G-N, _Sujeito2, Objeto2)-->
    objeto(G-N,Objeto2).    

ligador(Q2, _Sujeito2, _Accao2, _Objeto2, _N, Lista)-->
    pontuacao(_P),
    {responder(Q2, Lista)}.

ligador(Q2, Sujeito2, Accao2, Objeto2, N, Lista)-->
    ligador_aux,
    verbo(N, Accao),
    sujeito(_G-_N2, Sujeito),
    {resposta_concatenada(Lista, ListaNova, Accao, Sujeito)},
    ligador(Q2, Sujeito2, Accao2, Objeto2, N, ListaNova).

responder(qt, Lista):-
    length(Lista,Nlista),
    write('Resposta: '), write(Nlista),nl.

responder(nom, Lista):-
    write('Resposta: '),
    (verificar_igualdade(Lista), write('Verdade'),nl ; write('Falso'),nl). 

responder(_Q, Lista):-
    write('Resposta: '), write(Lista),nl.

ligador_aux-->
    e.

ligador_aux-->
    e,
    pron_int_esp_aux.  

ligador_aux-->
    pron_int_esp_aux.     

analise(Q, Sujeito, Accao, Objeto, N)-->
    sintg_interrogativo(Q,N,Sujeito, Objeto, Accao).

analise(Q, Sujeito, Accao, Objeto,N)-->
    {nominal(Q)},
    sintg_nominal(_G-N,Sujeito, Objeto, Accao).   

sintg_interrogativo(Q, N, Sujeito, Objeto, Accao)-->
    pron_int(Q, G-N),
    sintg_nominal(G-N,Sujeito, Objeto, Accao).

sintg_interrogativo(Q, N, Sujeito, Objeto, Accao)-->
    pron_lugar(Q, G-N),pron_lugar_aux,
    sintg_nominal(G-N,Sujeito, Objeto, Accao).  

sintg_interrogativo(Q, N, Sujeito, Objeto, Accao)-->
    pron_int_esp(Q, G-N),pron_int_esp_aux,
    sintg_que(G-N,Sujeito, Objeto, Accao).

sintg_que(G-N, Sujeito, _Objeto, Accao)-->
    sintg_nominal_que(G-N),
    verbo(N, Accao),
    sujeito(G-N, Sujeito).

sintg_nominal_que(G-N)-->
    pron_se(G-N,_Pron),
    verbo(N,_Accao).

%sintg_nominal_que(G-N, Sujeito1,_Objeto)-->
%    sujeito(G-N, Sujeito1).
%sintg_nominal_que(G-N, Sujeito1, Objeto)-->
%    sujeito(G-N, Sujeito1),
%    objeto(_G2-_N2, Objeto).

sintg_nominal(G-N, Sujeito, _Objeto, Accao)-->
    verbo(N,Accao),
    sujeito(G-N, Sujeito).

sintg_nominal(G-N, Sujeito, Objeto, Accao)-->
    verbo(N,Accao),
    sujeito(G-N, Sujeito),
    objeto(_G2-_N2, Objeto). 

sintg_nominal(G-N, Sujeito, Objeto, Accao)-->
    sujeito(G-N, Sujeito),
    verbo(N, Accao),
    objeto(_G2-_N2, Objeto).

sintg_nominal(G-N, Sujeito, _Objeto, Accao)-->
    sujeito(G-N, Sujeito),
    verbo(N, Accao).

sintg_nominal(G-N, Sujeito, Objeto, Accao)-->
    sujeito(G-N, Sujeito),
    objeto(_G2-_N2, Objeto),
    verbo(N, Accao).

sujeito(G-N, [Sujeito1|Sujeito2])-->
    sujeito_aux(G-N,Sujeito1),
    e,
    sujeito(_G2-_N2,Sujeito2).

sujeito(G-N, Sujeito)-->
    nome(G-N, Sujeito).

sujeito(G-N, Sujeito)-->
    det(G-N),
    nome(G-N, Sujeito).

sujeito(G-N, Sujeito)-->
    det(G-N),
    nome(G-N, Sujeito),
    adjetivo(G-N, _Adjetivo).

sujeito(G-N, Sujeito)-->
    det(G-N),
    adjetivo(G-N, _Adjetivo),
    nome(G-N, Sujeito).

sujeito_aux(G-N, Sujeito)-->
    nome(G-N, Sujeito).

sujeito_aux(G-N, Sujeito)-->
    det(G-N),
    nome(G-N, Sujeito).

sujeito_aux(G-N, Sujeito)-->
    det(G-N),
    nome(G-N, Sujeito),
    adjetivo(G-N, _Adjetivo).

sujeito_aux(G-N, Sujeito)-->
    det(G-N),
    adjetivo(G-N, _Adjetivo),
    nome(G-N, Sujeito).     

objeto(_G-_N, Objeto)-->
    nome(_G2-_N2, Objeto).

objeto(G-N, Objeto)-->
    prep(G-N),
    nome(G-N, Objeto).

objeto(_G-_N, [Objeto1|Objeto2])-->
    objeto_aux(_G2-_N2, Objeto1),
    e,
    objeto(_G3-_N3,Objeto2).

objeto_aux(_G-_N, Objeto)-->
    nome(_G2-_N2, Objeto).

objeto_aux(G-N, Objeto)-->
    prep(G-N),
    nome(G-N, Objeto).

e-->[e];['E'].

pontuacao(ponto)-->['.'].
pontuacao(interrogacao)-->['?'].
nominal(nom).

det(m-s)--> [o].
det(m-p)--> [os].
det(f-s)--> [a].
det(f-p)--> [as].
det(f-s)--> ['A'].
det(m-s)--> ['O'].
det(m-p)--> ['Os'].
det(f-p)--> ['As'].
det(_-_)--> [com].

prep(m-s)--> [no].
prep(m-p)--> [nos].
prep(f-s)--> [na].
prep(f-p)--> [nas].
prep(_-s)--> [de];['De'].
prep(m-s)-->[do].
prep(m-p)-->[dos].
prep(f-s)-->[da].
prep(f-p)-->[das].
prep(m-s)--> [ao].
prep(m-p)--> [aos].

adjetivo(m-s,tipico)--> ['típico'].
adjetivo(m-p,tipicos)--> ['típicos'].
adjetivo(f-s,tipica)--> ['típica'].
adjetivo(f-p,tipicas)--> ['típicas'].

adjetivo(m-s,tipico)--> [tipico].
adjetivo(m-p,tipicos)--> [tipicos].
adjetivo(f-s,tipica)--> [tipica].
adjetivo(f-p,tipicas)--> [tipicas].

pron_se(_-_, se)-->[se].
pron_int(qt,f-p)--> ['Quantas'];[quantas].
pron_int(qt,m-p)--> ['Quantos'];[quantos].
pron_int(ql,_-p)--> ['Quais'];[quais].
pron_int(ql,_-s)--> ['Quem'];[quem].
pron_lugar(onde,_-_)--> [de];['De'].
pron_lugar_aux--> [onde].
pron_int_esp(que,f-p)-->[a];['A'].
pron_int_esp(que,f-p)-->[o];['O']. 
pron_int_esp_aux-->[que].

nome(m-s,prato)-->[prato].
nome(m-p,pratos)-->[pratos].
nome(m-s,porto)-->['Porto'];[porto].
nome(f-s,lisboa)-->['Lisboa'];[lisboa].
nome(m-s,bacalhau)-->[bacalhau].
nome(m-s,ingradiente)-->[ingrediente].
nome(m-p,ingradientes)-->[ingredientes].
nome(f-s,francesinha)-->[francesinha].
nome(f-p,francesinhas)-->[francesinhas].
nome(f-p,migas)-->[migas].
nome(_-_,pao)-->[pao].
nome(_-s,refeicao)-->['refeição'];[refeicao].
nome(_-s,bolo)-->[bolo].
nome(f-p,migas)-->[migas].
nome(m-s,sangue)-->[sangue].
nome(f-s,parte)-->[parte].

verbo(p,ser)-->['são'];[sao].
verbo(p,existir)-->[existem].
verbo(s,inventar)-->[inventou].
verbo(_,poder)-->[pode].
verbo(_,cozinhar)-->[cozinhar].
verbo(s,pertence)-->[pertence].
verbo(s,levar)-->[leva].
verbo(p,levar)-->[levam].

ser(caracois, lisboa).
ser(francesinha, porto).
ser('tripas a moda do porto', porto).
ser('arroz de cabidela', porto).
ser(migas, alentejo).
existir('bacalhau a gomes de sa', bacalhau).
existir('bacalhau a lagareiro', bacalhau).
existir('bacalhau com migas', bacalhau).
ter(pao, migas).
ter(sangue, 'arroz de cabidela').
ter(pao, francesinha).
ter(queijo, francesinha).
ter(fiambre, francesinha).
inventar('Daniel David Silva', francesinha).
inventar('Jose Luis Gomes de Sa', 'bacalhau a gomes de sa').
inventar('Beiras', 'bacalhau a lagareiro').
inventar('Alentejanos aleatorios', migas).
pertence(bolo, sobremesa).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Respostas %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

resposta_concatenada(Lista, ListaNova, levar, Sujeito):-
    Facto1=..[ter, Sujeito, X],
    setof(X, (member(X, Lista), Facto1), ListaNova).

resposta_concatenada(Lista, ListaNova, cozinhar, Sujeito):-
    Facto1=..[ter, Sujeito, X],
    setof(X, (member(X, Lista), Facto1), ListaNova).

combina_respostas([], _Lista2, _ListaNova).

combina_respostas([Lista|RLis], Lista2, ListaNova):-
    member(Lista,Lista2),
    combina_respostas(RLis, Lista2, [Lista|ListaNova]).

combina_respostas([Lista|RLis], Lista2, ListaNova):-
    member(Lista,Lista2),
    combina_respostas(RLis, Lista2, ListaNova).

resposta(qt, [Sujeito|Resto], Accao, Objeto, [Lista|RList]):-
    nonvar(Resto),
    respostas_(Sujeito,Accao,Objeto ,RList),
    resposta(qt, Resto,Accao,Objeto ,Lista).

resposta(qt, Sujeito, Accao, [Objeto|Resto], [Lista|RList]):-
    nonvar(Resto),
    respostas_(Sujeito,Accao,Objeto ,RList),
    resposta(qt, Sujeito,Accao,Resto ,Lista).

resposta(ql,[Sujeito|Resto],Accao,Objeto, [Lista|RList]):-
    nonvar(Resto),
    respostas_(Sujeito,Accao,Objeto ,RList),
    resposta(ql, Resto,Accao,Objeto ,Lista).

resposta(ql,Sujeito,Accao,[Objeto|Resto],[Lista|RList]):-
    nonvar(Resto),
    respostas_(Sujeito,Accao,Objeto ,RList),
    resposta(ql, Sujeito,Accao,Resto ,Lista).

resposta(onde, Sujeito, Accao, [Objeto|Resto], [Lista|RList]):-
    nonvar(Resto),
    respostas_onde(Sujeito,Accao,Objeto ,RList),
    resposta(onde,Sujeito, Accao, Resto, Lista).

resposta(onde, [Sujeito|Resto], Accao, Objeto, [Lista|RList]):-
    nonvar(Resto),
    respostas_onde(Sujeito,Accao,Objeto ,RList),
    resposta(onde,Resto, Accao, Objeto, Lista).

resposta(que, Sujeito, Accao, [Objeto|Resto], [Lista|RList]):-
    nonvar(Resto),
    respostas_que(Sujeito,Accao,Objeto ,RList),
    resposta(que,Sujeito, Accao, Resto, Lista).

resposta(que, [Sujeito|Resto], Accao, Objeto, [Lista|RList]):-
    nonvar(Resto),
    respostas_que(Sujeito,Accao,Objeto ,RList),
    resposta(que,Resto, Accao, Objeto, Lista).

resposta(nom,[Sujeito|Resto], Accao, Objeto, [Lista|RList]):-
    nonvar(Resto),
    respostas_nom(Sujeito, Accao, Objeto, RList),
    resposta(nom,Resto, Accao, Objeto, Lista).

resposta(nom,Sujeito, Accao, [Objeto|Resto], [Lista|RList]):-
    nonvar(Resto),
    respostas_nom(Sujeito, Accao, Objeto, RList),
    resposta(nom,Sujeito, Accao, Resto, Lista).

resposta(qt, Sujeito, Accao, Objeto, Lista):-
    respostas_(Sujeito,Accao,Objeto ,Lista).

resposta(ql,Sujeito,Accao,Objeto,Lista):-
    respostas_(Sujeito,Accao,Objeto,Lista).

resposta(onde, Sujeito, Accao, Objeto, Lista):-
    respostas_onde(Sujeito,Accao,Objeto ,Lista).

resposta(que, Sujeito, Accao, Objeto, Lista):-
    respostas_que(Sujeito,Accao, Objeto ,Lista).

resposta(nom,Sujeito,Accao,Objeto,Lista):-
    respostas_nom(Sujeito, Accao, Objeto, Lista).

respostas_nom(Sujeito, levar, Objeto, Lista):-
    Facto1=..[ter, X, Sujeito],
    setof(X, Facto1, ListaTemp),
    ( member(Objeto, ListaTemp), adiciona_sim(Lista) ; adiciona_nao(Lista) ).    

respostas_onde(Sujeito,Accao,_Objeto,Lista):-
    Facto1=..[Accao, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).

respostas_que(Sujeito,cozinhar, _Objeto, Lista):-
    Facto1=..[ter, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).

respostas_que(Sujeito,Accao, _Objeto, Lista):-
    Facto1=..[Accao, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).    

respostas_(Sujeito,Accao,Objeto,Lista):-
    var(Objeto), % quando variavel esta livre
    nonvar(Sujeito),
    Facto1=..[Accao,X,Sujeito],
    ( setof( X, Facto1, Lista) ; Lista=[] ).  

respostas_(_Sujeito,Accao,Objeto,Lista):-
    nonvar(Objeto),
    Facto1=..[Accao,X,Objeto],
    ( setof( X, Facto1, Lista) ; Lista=[] ).  

adiciona_sim(sim).
adiciona_nao(nao).

verificar_igualdade([A|R]):-
    A = sim,
    verificar_igualdade(R).

verificar_igualdade(A):-
    A = sim.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Fazer lista %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transf_lista(Frase,Lista_Pal):-
    string_to_list(Frase,Lista_char),
    faz_palavras(Lista_char,[],Lista_Pal).
    
faz_palavras([],L_char1,[Pal1]):-
    atom_chars(Pal1,L_char1).
 
faz_palavras([32|T],L_char1,[Pal1|T1]):-
    atom_chars(Pal1,L_char1),
    faz_palavras(T,[],T1).

faz_palavras([X|T],L_char,L_Pal):-
    append(L_char,[X],L_char1),
    faz_palavras(T,L_char1,L_Pal).