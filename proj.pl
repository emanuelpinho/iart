:- dynamic erro/1.
%  O programa deve ser capaz de aceitar e responder a frases do tipo (lista não exaustiva):

%  1- Quais são os pratos típicos do Porto?     CERTO
%  2- Quem inventou a francesinha?              CERTO
%  3- Quantos pratos de bacalhau existem?       CERTO MAS LOOP QUE NAO PARA
%  4- E o bacalhau à Gomes de Sá?
%  5- O que se pode cozinhar com pão?
%  6- De onde são as migas?                     CERTO MAS LOOP QUE NAO PARA
%  7- O arroz de cabidela leva sangue?

%	Escreva uma frase do tipo:
%	"hoje esta sol .".

inicio:-
    write('Escreva uma frase: '),nl,
    read(Frase), %fread(s,0,0,Frase)
    transf_lista(Frase,Lista_Pal),
    verifica_frase(_Q,Q2,_Sujeito,Sujeito2,_Accao, Accao2,_Objeto, Objeto2, Lista_Pal,[]),
    ciclo_de_jogo(Q2,_Q3,Sujeito2,_Sujeito3,Accao2, _Accao3,Objeto2, _Objeto3).

ciclo_de_jogo(Q,Q2,Sujeito,Sujeito2,Accao, Accao2,Objeto, Objeto2):- nl,
	write('Escreva uma frase: '),nl,
	read(Frase), %fread(s,0,0,Frase)
	transf_lista(Frase,Lista_Pal),
    verifica_frase(Q,Q2,Sujeito,Sujeito2,Accao, Accao2,Objeto, Objeto2, Lista_Pal,[]),
    ciclo_de_jogo(Q2,_Q3,Sujeito2,_Sujeito3,Accao2, _Accao3,Objeto2, _Objeto3).

%	( verifica_frase(Lista_Pal,[]) ; 
%		( erro(semantico),write('erro semantico') ; 
%			write('erro sintatico') ) ).


verifica_frase(Q,_Q2,_Sujeito,Sujeito2,Accao, _Accao2,_Objeto, Objeto2)--> e,
    sintg_interrogativo_e(Sujeito2,Accao,Objeto2),
    {resposta(Q,Sujeito2,Accao,Objeto2)}.

verifica_frase(_Q,Q2,_Sujeito,Sujeito2,_Accao, Accao2,_Objeto, Objeto2)--> analise(Q2,Sujeito2, Accao2, Objeto2),
    {resposta(Q2,Sujeito2,Accao2,Objeto2)}.

analise(Q, Sujeito, Accao, Objeto)-->
    pron,
    sintg_interrogativo_ond(Q,_N,Sujeito, Objeto, Accao).

analise(Q, Sujeito, Accao, _Objeto)--> det(_-_),
    que(Q),
    sintg_interrogativo_que(Sujeito, Accao).

analise(Q, Sujeito, Accao, Objeto)-->
    sintg_interrogativo(Q,_N,Sujeito, Objeto, Accao).

analise(_Q,Sujeito,Accao,Objeto)-->
    sintg_nominal(Sujeito, Accao, Objeto).  

sintg_interrogativo_e(Sujeito,_Accao,_Objeto)-->
    det(G-N),
    nome(G-N, Sujeito),
    pontuacao.

sintg_interrogativo_e(_Sujeito,_Accao,Objeto)-->
    prep(G-N),
    nome(G-N, Objeto),
    pontuacao.    

sintg_interrogativo_que(Sujeito, Accao)-->
    pronome,
    verbo(_,_,_),
    sintg_verbal(_,Sujeito,Accao).

sintg_interrogativo_que(Sujeito, Accao)-->
    complemento(N,_),
    sintg_verbal(N,Sujeito,Accao).

sintg_interrogativo_ond(Q,N,Sujeito, _Objeto, Accao)-->pron_int(Q, _G-N),
    sintg_verbal(N,Sujeito,Accao).

sintg_interrogativo(Q, N, Sujeito, Objeto, Accao)-->pron_int(Q, _G-N),
    sintg_verbal(N,Sujeito,Accao),
    complemento_pos_nome(Objeto),
    pontuacao.

sintg_interrogativo(Q, N, Sujeito, _Objeto, Accao)-->pron_int(Q, _G-N),
    sintg_verbal(N,Sujeito,Accao),
    pontuacao.

sintg_interrogativo(Q, N, Sujeito, Objeto, Accao)-->pron_int(Q, G-N),
    nome(G-N,Sujeito),
    complemento_pos_nome(Objeto),
    sintg_verbal(N, Accao).    

sintg_nominal(Sujeito, Accao, Objeto)-->
    det(G-N),
    nome(G-N, Sujeito),
    adjetivo(G-N, Sujeito),
    verbo(N,_,Accao),
    nome(G-N, Objeto),
    pontuacao.

sintg_nominal(Sujeito, Accao, Objeto)-->
    det(G-N),
    nome(G-N, Sujeito),
    verbo(N,_,Accao),
    nome(_-_, Objeto),
    pontuacao.    

sintg_verbal(N, Sujeito, Accao)-->verbo(N,_,Accao),
    complemento(Sujeito). 

sintg_verbal(N, Accao)-->verbo(N,_,Accao).

complemento_pos_nome(Objeto)-->complementos_aux(G-N),
    nome(G-N, Objeto).

complemento(Sujeito)-->complementos_aux(G-N),
    nome(G-N,Sujeito),
    adjetivo(G-N, _Adj).

complemento(N2,Sujeito)-->adjetivo(G-_N, _Adj),
    complementos_aux(G-N2),
    nome(G-N2,Sujeito).

complemento(Sujeito)-->complementos_aux(G-N),
    nome(G-N,Sujeito).   

complementos_aux(G-N)-->prep(G-N).

complementos_aux(G-N)-->det(G-N).

complementos_aux(_G-_N)-->outros.

complementos_aux(_G-_N).

% sintg_nominal(G-N,Sujeito, Objeto)-->nome(G-N, Sujeito), 
    % adjetivo(G-N, Adj), 
    % complemento2(Objeto).

% sintg_nominal(G-N, Objeto)-->nome(G-N, Sujeito),
    % adjetivo(G-N, Adj).

% sintg_nominal-->nome(G-N, Sujeito).

% sintg_nominal-->nome, [e], nome.    
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gramática %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

e-->[e];['E'].
que(que)-->[que].
outros-->[com].
pron-->['De'];[de].
pron_int(qt,f-p)--> ['Quantas'];[quantas].
pron_int(qt,m-p)--> ['Quantos'];[quantos].
pron_int(ql,_-p)--> ['Quais'];[quais].
pron_int(ql,_-s)--> ['Quem'];[quem].
pron_int(onde,_-_)--> ['Onde'];[onde].

pronome-->[se].

det(m-s)--> [o].
det(m-p)--> [os].
det(f-s)--> [a].
det(f-p)--> [as].
det(f-s)--> ['A'].
det(m-s)--> ['O'].
det(m-p)--> ['Os'].
det(f-p)--> ['As'].

prep(m-s)--> [no].
prep(m-p)--> [nos].
prep(f-s)--> [na].
prep(f-p)--> [nas].
prep(_-s)--> [de];['De'].
prep(m-s)-->[do].
prep(m-p)-->[dos].
prep(f-s)-->[da].
prep(f-p)-->[das].
prep(m-s)--> [ao].
prep(m-p)--> [aos].

adjetivo(m-s,tipico)--> ['típico'].
adjetivo(m-p,tipicos)--> ['típicos'].
adjetivo(f-s,tipica)--> ['típica'].
adjetivo(f-p,tipicas)--> ['típicas'].

adjetivo(_-s,parte)-->[parte].
adjetivo(m-s,tipico)--> [tipico].
adjetivo(m-p,tipicos)--> [tipicos].
adjetivo(f-s,tipica)--> [tipica].
adjetivo(f-p,tipicas)--> [tipicas].

nome(m-s,prato)-->[prato].
nome(m-p,pratos)-->[pratos].
nome(m-s,porto)-->['Porto'];[porto].
nome(m-s,bacalhau)-->[bacalhau].
nome(m-s,ingradiente)-->[ingrediente].
nome(m-p,ingradientes)-->[ingredientes].
nome(f-s,francesinha)-->[francesinha].
nome(f-p,francesinhas)-->[francesinhas].
nome(f-p,migas)-->[migas].
nome(_-_,pao)-->[pao].
nome(_-s,refeicao)-->['refeição'];[refeicao].
nome(_-s,bolo)-->[bolo].
nome(f-p,migas)-->[migas].
nome(m-s,sangue)-->[sangue].

verbo(p,_,ser)-->['são'];[sao].
verbo(p,_,existir)-->[existem].
verbo(s,_,inventar)-->[inventou].
verbo(_,_,poder)-->[pode].
verbo(_,_,cozinhar)-->[cozinhar].
verbo(s,_,pertence)-->[pertence].
verbo(s,_,levar)-->[leva].
verbo(p,_,levar)-->[levam].

%%%%%%% NOTAS %%%%%%%
verbo(s,Sujeito,ter)--> [tem],{prato(Sujeito);assert(erro(semantico)),fail}.
verbo(p,Sujeito,ser)--> [sao],{prato(Sujeito);assert(erro(semantico)),fail}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%% Base de conhecimento %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ser(francesinha, porto).
ser('tripas à moda do porto', porto).
ser(migas, alentejo).
existir('bacalhau à gomes de sá', bacalhau).
existir('bacalhau à lagareiro', bacalhau).
existir('bacalhau com migas', bacalhau).
ter(pao, migas).
ter(pao, francesinha).
ter(queijo, francesinha).
ter(fiambre, francesinha).
inventar('Daniel David Silva', francesinha).
inventar('José Luís Gomes de Sá', 'bacalhau à gomes de sá').
inventar('José Luís Gomes de Sá', 'bacalhau a gomes de sa').
inventar('Beiras', 'bacalhau à lagareiro').
inventar('Beiras', 'bacalhau a lagareiro').
inventar('Alentejanos aleatorios', migas).
pertence(bolo, sobremesa).


prato(francesinha).
prato('tripas à moda do porto').
prato(migas).
prato('bacalhau à gomes de sá').
prato('bacalhau à lagareiro').
prato('bacalhau com migas').
prato([]).

prato([(Nome,_)|R]):- prato(Nome),prato(R). 
prato((Nome,_)):- prato(Nome).

pontuacao--> ['?'].
pontuacao--> ['.'].

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Respostas %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

resposta(Q,Sujeito,Accao,Objeto):- 
    var(Q),
    respostas_nom(Sujeito,Accao,Objeto).

resposta(Q,Sujeito,Accao,Objeto):- Q=ql,
    respostas_(Sujeito,Accao,Objeto,Lista),
    write('RESPOSTA1'),nl, 
    write(Lista), nl.

resposta(Q,Sujeito,Accao,Objeto):- Q=qt,
    respostas_(Sujeito,Accao,Objeto,Lista),
    length(Lista,Nlista),
    write('RESPOSTA2'),nl, write(Nlista), nl.

resposta(Q,Sujeito,Accao,Objeto):- Q=onde,
    respostas_onde(Sujeito,Accao,Objeto,Lista),
    write('RESPOSTA3'),nl,  
    write(Lista), nl.

resposta(Q,Sujeito,Accao,_Objeto):- Q=que,
    respostas_que(Sujeito,Accao, Lista),
    write('RESPOSTA4'),nl,  
    write(Lista), nl.         

%resposta(Q,Sujeito,Accao,Objeto):- respostas_(Sujeito,Accao,Objeto,Lista), 
%    ( ( Q=ql,write(Lista) ) ; ( length(Lista,Nlista),write(Nlista) ) ), nl.

%respostas_(Sujeito,Accao,Objecto,Lista):- Sujeito=(Nome,Adj),
%    var(Adj), 
%    Facto=..[Accao,X,Objecto], 
%    ( setof( X, Facto, Lista) ; Lista=[] ). 

respostas_que(Sujeito,Accao,Lista):-
    Accao=cozinhar,
    Facto1=..[ter, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).

respostas_que(Sujeito,Accao,Lista):-
    Facto1=..[Accao, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).    

respostas_onde(Sujeito,Accao,Objeto,Lista):-
    var(Objeto),
    Facto1=..[Accao, Sujeito, X],
    ( setof(X, Facto1, Lista); Lista=[]).

respostas_nom(Sujeito,Accao,Objeto):-
    Accao = levar,
    Facto1=..[ter, X, Sujeito],
    setof(X, Facto1, Lista),
    ( member(Objeto, Lista), write('Sim'),nl ; write('Nao') ). 

respostas_(Sujeito,Accao,Objeto,Lista):-
    var(Objeto), % quando variavel esta livre
    nonvar(Sujeito),
    Facto1=..[Accao,X,Sujeito],
    ( setof( X, Facto1, Lista) ; Lista=[] ).  

respostas_(_Sujeito,Accao,Objeto,Lista):-
    nonvar(Objeto),
    Facto1=..[Accao,X,Objeto],
    ( setof( X, Facto1, Lista) ; Lista=[] ).  


resposta_afirm([],_,_):- write(concordo). 
resposta_afirm([(Nome1,_)|ONomes],Accao,Objecto):-
    Facto=..[Accao,Nome1,Objecto],
    Facto, 
    resposta_afirm(ONomes,Accao,Objecto).

resposta_afirm(_,_,_):- write(discordo).  
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Fazer lista %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

transf_lista(Frase,Lista_Pal):-
    string_to_list(Frase,Lista_char),
    faz_palavras(Lista_char,[],Lista_Pal).
    
faz_palavras([],L_char1,[Pal1]):-
    atom_chars(Pal1,L_char1).
 
faz_palavras([32|T],L_char1,[Pal1|T1]):-
    atom_chars(Pal1,L_char1),
    faz_palavras(T,[],T1).

faz_palavras([X|T],L_char,L_Pal):-
    append(L_char,[X],L_char1),
    faz_palavras(T,L_char1,L_Pal).
