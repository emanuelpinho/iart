:- dynamic erro/1.

ln:- write('Escreva uma frase: '),nl, 
    fread(s,0,0,Frase),
    transf_lista(Frase,Lista_Pal), 
    ( verifica_frase(Lista_Pal,[]) ;
    ( erro(semantico),write('erro semantico') ; write('erro sintatico'))).

verifica_frase--> sintg_interrogativo(Q,N,Sujeito), 
    sintg_verbal(N,Sujeito,Accao,Objecto), 
    {resposta(Q,Sujeito,Accao,Objecto)}.

verifica_frase --> sintg_nominal(_-N,LSujeito),
    sintg_verbal(N,LSujeito,Accao,Objecto), 
    {resposta_afirm(LSujeito,Accao,Objecto)}.

sintg_interrogativo(Q,N,Sujeito)--> pron_int(Q,G-N), 
    det(G-N),
    sintg_nominal_r(G-N,Sujeito),
    [que]. 

sintg_interrogativo(Q,N,Sujeito)--> pron_int(Q,G-N),
    sintg_nominal_r(G-N,Sujeito).

sintg_nominal(G-p,[Nome1|[RNome]])--> sintg_nominal1(_,Nome1),[e], sintg_nominal1(_,RNome).

sintg_nominal(G-N,[Nome])--> sintg_nominal1(G-N,Nome).

sintg_nominal1(G-N,Sujeito)--> det(G-N),
    sintg_nominal_r(G-N,Sujeito). 

sintg_nominal1(G-N,Sujeito)--> sintg_nominal_r(G-N,Sujeito). 
sintg_nominal_r(G-N,(Nome,Adj))--> nome(G-N,Nome), adjectivo(G-N,Adj). 
sintg_nominal_r(G-N,(Nome,_))--> nome(G-N,Nome).
sintg_verbal(N,Sujeito,Accao,Objecto)--> verbo(N,Sujeito,Accao), complemento(Objecto).
complemento(Obj)--> prep(G-N),nome(G-N,Obj). 
complemento(Obj)--> prep(G-N),nome(G-N,_),adjectivo(G-N,Obj).



%%%%%%%%%%%%%%%%%%%%%%%% RESPOSTAS %%%%%%%%%%%%%%%%%%%%%%%%

% Quantas equipas latinas jogam no campeonato europeu 
% Quais as equipas latinas que jogam no campeonato europeu

resposta(Q,Sujeito,Accao,Objecto):- respostas_(Sujeito,Accao,Objecto,Lista), 
    ( ( Q=ql,write(Lista) ) ; ( length(Lista,Nlista),write(Nlista) ) ), nl.

respostas_(Sujeito,Accao,Objecto,Lista):- Sujeito=(Nome,Adj),
    var(Adj), 
    Facto=..[Accao,X,Objecto], 
    ( setof( X, Facto, Lista) ; Lista=[] ). 

respostas_(Sujeito,Accao,Objecto,Lista):- Sujeito=(Nome,Adj),
    nonvar(Adj), 
    Facto1=..[ser,X,Adj], 
    Facto2=..[Accao,X,Objecto], 
    ( setof( X, (Facto1,Facto2), Lista) ; Lista=[] ).

resposta_afirm([],_,_):- write(concordo). 
resposta_afirm([(Nome1,_)|ONomes],Accao,Objecto):-
    Facto=..[Accao,Nome1,Objecto],
    Facto, 
    resposta_afirm(ONomes,Accao,Objecto).

resposta_afirm(_,_,_):- write(discordo).


%%%%%%%%%%%%%%%%%%%%%%%% GRAMÁTICA %%%%%%%%%%%%%%%%%%%%%%%%

pron_int(qt,f-p)--> ['Quantas'];[quantas].
pron_int(ql,_-p)--> ['Quais'];[quais].
det(m-s)--> [o].
det(f-s)--> [a].
det(f-p)--> [as].
det(f-s)--> ['A'].
det(m-s)--> ['O'].
prep(m-s)--> [no].
prep(m-s)--> [ao].
adjectivo(m-s,europeu)--> [europeu].
adjectivo(f-p,latino)--> [latinas].
nome(f-p,equipa)--> [equipas].
nome(m-s,campeonato)--> [campeonato].
nome(m-s,portugal)--> ['Portugal'];[portugal].
nome(m-s,brasil)--> ['Brasil']; [brasil].
nome(f-s,franca)--> ['Franca'].
nome(f-s,alemanha)--> ['Alemanha'];[alemanha].
nome(m-s,grupoA)-->[grupoA].
verbo(p,Sujeito,jogar)--> [jogam].
verbo(s,Sujeito,jogar)--> [joga].
verbo(p,Sujeito,pertencer)--> [pertencem],{equipa(Sujeito);assert(erro(semantico)),fail}. 
verbo(s,Sujeito,pertencer)--> [pertence],{equipa(Sujeito);assert(erro(semantico)),fail}.


%%%%%%%%%%%%%%%%%%%%%%%% BASE CONHECIMENTO %%%%%%%%%%%%%%%%%%%%%%%%


ser(portugal,latino). 
ser(espanha,latino). 
ser(italia,latino).
ser(franca,latino). 
ser(romenia,latino). 
ser(noruega,latino).
pertencer(portugal,grupoA). 
pertencer(alemanha,grupoA). 
jogar(portugal,europeu). 
jogar(alemanha,europeu). 
jogar(franca,europeu).
equipa(equipa).
equipa(portugal).
equipa(alemanha).
equipa(brasil).
equipa([]).
equipa([(Nome,_)|R]):- equipa(Nome),equipa(R). 
equipa((Nome,_)):- equipa(Nome).


transf_lista(Frase,Lista_Pal):- string_chars(Frase,Lista_char), faz_palavras(Lista_char,[],Lista_Pal).
faz_palavras([13],L_char1,[Pal1]):- atom_chars(Pal1,L_char1).
faz_palavras([32|T],L_char1,[Pal1|T1]):- atom_chars(Pal1,L_char1), faz_palavras(T,[],T1).
faz_palavras([X|T],L_char,L_Pal):- append(L_char,[X],L_char1), faz_palavras(T,L_char1,L_Pal).




